// / <reference types="Cypress" />

context("Home page", () => {
    beforeEach(() => {
      cy.visit("/");
    });
  
    it("Link to react docs exist", () => {
      cy.get("a").should("contain", "Learn React");
    });
  });
  